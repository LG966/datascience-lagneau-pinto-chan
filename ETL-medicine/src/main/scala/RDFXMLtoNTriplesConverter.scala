import java.io.{File, FileOutputStream}
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.riot.{Lang, RDFDataMgr}
import org.apache.jena.util.FileManager

object RDFXMLtoNTriplesConverter {
  def main(args: Array[String]): Unit = {
    val inputFile = new File("./../data/cipUCDfev23.rdf")
    val outputFile = new File("./../data/cipUCDfev23.nt")

    // create an RDF model from the RDF/XML file
    val model = ModelFactory.createDefaultModel()
    val inputStream = FileManager.getInternal().open(inputFile.getAbsolutePath)
    model.read(inputStream, null)

    // write the model to the output file in RDF/N-Triples format
    val outputStream = new FileOutputStream(outputFile)
    RDFDataMgr.write(outputStream, model, Lang.NTRIPLES)
  }
}
