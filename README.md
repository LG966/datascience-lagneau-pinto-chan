# <center>M2 LID – UGE </center>
# <center> Projet Introduction à la Data Science </center>

## Membres du groupe

CHAN Mathieu

LAGNEAU Gaétan (***Responsable***)

PINTO Bryan

## Taux de contributions

* CHAN Mathieu : +1.75
* LAGNEAU Gaétan : +1.75
* PINTO Bryan : +1.75

## Questions

`Q1: Motiver le format RDF sélectionné et le choix de l’abstraction Spark.`

Le format RDF/XML n'étant pas évident à cause de sa syntaxe assez complexe,
nous avons opté pour le format RDF/NT qui nous semble plus adapté. En effet, le
format est plus simple à lire car il est basé sur des triplés (Sujet / Prédicat
/ Objet) représenté ligne par ligne. De plus, les données seront plus simples à
traitées grâce à l'utilisation des requêtes SPARQL / JENA. 

`Q2: combien avez-vous de tuples dans votre résultat`

793591

## Nettoyage du fichier cisCip.txt

Afin de pouvoir traiter le fichier cisCip.txt, nous avons appliquer le schéma suivant:
```SCALA
val schema = new StructType()
      .add("cis",IntegerType,true)
      .add("cip7",IntegerType,true)
      .add("lib",StringType,true)
      .add("statut",StringType,true)
      .add("comm",StringType,true)
      .add("date",DateType,true)
      .add("cip13",LongType,true)
      .add("agr",StringType,true)
      .add("remb",StringType,true)
      .add("prixbase",FloatType,true)
      .add("prixfinal",FloatType,true)
      .add("taxe",FloatType,true)
```

En l'appliquant, on s'est rendu compte que toutes les entrées était considérées
comme fausses à cause du formatage des float (',' pour le format français et '.'
pour le format anglais).

Après avoir corrigé cela, nous avons remarqué que certaines valeurs étaient
nulles et que le caractère 'é' n'était pas bien encodé. Nous avons donc remplacé
tout les 'é' par 'e'.

On vérifie si des erreurs sont présentes :
```SQL
-- Prix negatif
select * from `cisCip_txt` where prixbase < 0 or prixfinal < 0 or taxe < 0;

-- Prix final ne corresspond pas au prix de base
select cis, cip7, prixfinal, ROUND(sum(prixbase + taxe),2) as tot from `cisCip_txt` GROUP BY cis, cip7, prixfinal having tot != prixfinal;

-- Data corrompue
display(df1.filter(df1("cip7").isNull || df1("cis").isNull || df1("lib").isNull || df1("statut").isNull || df1("comm").isNull || df1("date").isNull || df1("cip13").isNull))
df1.filter(df1("info").isNotNull).count()
```
<br/>
<br/>

`Q3: En fonction de la répartition des données, est-il possible de supprimer le
symbole ‘%’ de la colonne taux de remboursement. Présenter clairement votre
méthode.`

Afin de supprimer le '%' de la colonne taux de remboursement nous avons recréer
un DF à partir du premier puis appliquer à l'aide d'un map la fonction
`stripSuffix("%")`  sur le champs *taux de remboursement* :

```SCALA
val df2 = df1.map(x => (
  x.getInt(0),
  x.getInt(1),
  x.getString(2),
  x.getString(3),
  x.getString(4),
  x.getDate(5),
  x.getLong(6),
  x.getString(7),
  x.getString(8).stripSuffix("%"),
  x.getFloat(9),
  x.getFloat(10),
  x.getFloat(11)
)).toDF("cis","cip7","lib","statut","comm","date","cip13","agr","remb","prixbase","prixfinal","taxe")
```

`Vous devez effectuer plusieurs filtres:`
```SCALA
val filter = df3.select("*").where("statut = 'Presentation active' and comm = 'Declaration de commercialisation' and prixfinal > 0")
```

`Question Q4: Combien de tuples garder vous depuis la source cisCompo?`

On garde 20132 tuples depuis cisCompo

`Question Q6`

```
count : 322
correct prediction : 322
accuracy : 100.0%
```

`Question Q7`

```
count : 401
correct prediction : 401
accuracy : 100.0%
```

`Question Q8`

```
count : 424
correct prediction : 423
accuracy : 99.76415%
```

`Question Q9`

```
count : 450
correct prediction : 439
accuracy : 97.55556%
```

`Q10: En fonction des problèmes rencontrés dans cette expérimentation, expliquez
pourquoi la précision de la prédiction s’est dégradée.`

Plus il y a de données, plus il est difficile d'avoir un modèle précis pour
couvrir l'ensemble des cas, parce que les données deviennent de plus en plus
diverses.

`Q11: En fonction des problèmes rencontrés dans cette expérimentation, expliquez
pourquoi la précision de la prédiction s’est dégradée.`

Pour cette partie, on a utiliser les paires suivantes de (idclass, codeSub):

* (650, 2709)
* (605, 1182) 

on obtient respectivement: 

```
count : 474
correct prediction : 463
accuracy : 97.67932%
```

et

```
count : 479
correct prediction : 468
accuracy : 97.703545%
```

**Note : Les arbres de décisions sont affichés sur le notebook**

## Notebooks

[first file (cipUCDfev23)](https://databricks-prod-cloudfront.cloud.databricks.com/public/4027ec902e239c93eaaa8714f173bcfc/4135816532192350/4492230388246219/8071798844004111/latest.html)

[second file (cisCip)](https://databricks-prod-cloudfront.cloud.databricks.com/public/4027ec902e239c93eaaa8714f173bcfc/6683079121235654/756405965229513/8634908499904957/latest.html)

[third file (cisCompo)](https://databricks-prod-cloudfront.cloud.databricks.com/public/4027ec902e239c93eaaa8714f173bcfc/4135816532192350/1457208331635434/8071798844004111/latest.html)

[fourth file (speclass) & ML](https://databricks-prod-cloudfront.cloud.databricks.com/public/4027ec902e239c93eaaa8714f173bcfc/6683079121235654/756405965229540/8634908499904957/latest.html)

[fifth file (speclass)](https://databricks-prod-cloudfront.cloud.databricks.com/public/4027ec902e239c93eaaa8714f173bcfc/6683079121235654/756405965229540/8634908499904957/latest.html)

[sixth file](https://databricks-prod-cloudfront.cloud.databricks.com/public/4027ec902e239c93eaaa8714f173bcfc/4135816532192350/646685757106822/8071798844004111/latest.html)
