ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.10"

lazy val root = (project in file("."))
  .settings(
    name := "ETL-medicine"
  )

libraryDependencies += "org.apache.jena" % "apache-jena-libs" % "4.3.0"
